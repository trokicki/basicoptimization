﻿using System;
using NUnit.Framework;
using Optimizers.Exceptions;
using Optimizers.Helpers;
using Optimizers.Models;

namespace OptimizersTests.Helpers
{
	[TestFixture]
	public class FibonacciNumbersStreamTests
	{
		private FibonacciNumbersProvider _numbersProvider;

		[SetUp]
		public void SetUp()
		{
			_numbersProvider = new FibonacciNumbersProvider();
		}

		[TestCase(0, 1)]
		[TestCase(1, 1)]
		[TestCase(2, 2)]
		[TestCase(3, 3)]
		[TestCase(4, 5)]
		[TestCase(5, 8)]
		[TestCase(6, 13)]
		[TestCase(7, 21)]
		public void GetNumber_Returns1_ForFirstItem(int index, int expectedValue)
		{
			Assert.That(_numbersProvider.GetNumber(index), Is.EqualTo(expectedValue));
		}
	}

	[TestFixture]
	public class BinarySearchTests
	{
		private const double Epsilon = 0.0001;

		[Test]
		public void SearchXFor_ReturnsCorrectX_ForLinearArisingFunction()
		{
			//Arrange
			var func = new SingleVarFunction(x => x + 2);
			var finder = new BinarySearch(func, Epsilon);

			//Act
			var found = finder.SearchXFor(4.0, Range.Normalized(-2, 40));

			//Assert
			Assert.That(found, Is.EqualTo(2).Within(Epsilon));
		}

		[Test]
		public void SearchXFor_ReturnsCorrectX_ForLinearNonArisingFunction()
		{
			//Arrange
			var func = new SingleVarFunction(x => -x + 2);
			var finder = new BinarySearch(func, Epsilon);

			//Act
			var found = finder.SearchXFor(3.0, Range.Normalized(-50, 50));

			//Assert
			Assert.That(found, Is.EqualTo(-1).Within(Epsilon));
		}

		[Test]
		public void SearchXFor_ThrowsIterationLimitExceededException_WhenCannotFindX()
		{
			//Arrange
			var func = new SingleVarFunction(x => x);
			var finder = new BinarySearch(func, Epsilon);

			//Act & Assert
			Assert.Throws<IterationLimitExceededException>(() => { finder.SearchXFor(-1, new Range(0, 10)); });
		}
	}
}
