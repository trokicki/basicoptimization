﻿using System;
using NUnit.Framework;
using Optimizers;
using Optimizers.Exceptions;
using Optimizers.Models;
using Optimizers.RangeFinders;

namespace OptimizersTests.RangeFinders
{
	[TestFixture]
	public class RangeFinderTests
	{
		private readonly Random _random = new Random();
		private readonly ISingleVarFunction _parabolicFunction = new SingleVarFunction(x => x*x);
		private readonly ISingleVarFunction _sophisticatedFunction = new SingleVarFunction(x => -Math.Cos(x) * Math.Exp(-Math.Pow(x - 2 * Math.PI, 2)) + 0.002 * Math.Pow(x, 2));
		public const double SophisticatedFunctionProperValue = 6.27481807546061;

		#region Simple parabolic function
		[TestCase(1.00000001)]
		[TestCase(1.2)]
		[TestCase(1.84234)]
		[TestCase(1.99999999999)]
		[TestCase(40)]
		[TestCase(32567)]
		public void FindRange_FindsRangeForParabolicFunction_WhenStartingInZero(double expansionParam)
		{
			//Arrange
			var rangeFinder = new RangeFinder(_parabolicFunction, expansionParam);
			var originalRange = GetRandomRange();

			//Act
			var result = rangeFinder.Find(originalRange, 0);

			//Assert
			Assert.That(result.ContainsInclusive(0f));
		}

		[TestCase(1.00000001)]
		[TestCase(1.2)]
		[TestCase(1.84234)]
		[TestCase(1.99999999999)]
		[TestCase(40)]
		[TestCase(32567)]
		public void FindRange_FindsRangeForParabolicFunction_WhenStartingInRandomPlace(double expansionParam)
		{
			//Arrange
			var rangeFinder = new RangeFinder(_parabolicFunction, expansionParam);
			var originalRange = GetRandomRange();
			var startingPoint = GetRandomWithinRange(originalRange);

			//Act
			var result = rangeFinder.Find(originalRange, startingPoint);

			//Assert
			Assert.That(result.ContainsInclusive(0f), String.Format("Minimum not found in range {0} starting in {1} [actual range: {2}]", originalRange, startingPoint, result));
		}

		[Test]
		public void FindRange_DoesNotReturnReverseRange_RandomRanges()
		{
			for (var i = 0; i < 100; i++)
			{
				var expansionParam = _random.NextDouble() + 1;
				var rangeFinder = new RangeFinder(_parabolicFunction, expansionParam);
				var originalRange = GetRandomRange();
				var startingPoint = GetRandomWithinRange(originalRange);
				var result = rangeFinder.Find(originalRange, startingPoint);
				Assert.That(result.Start, Is.LessThanOrEqualTo(result.End), 
					"Invalid range: {0} for original range: {1}, starting point: {2}, expansionParam: {3}", 
					result, originalRange, startingPoint, expansionParam);
			}
		}

		[Test]
		public void FindRange_DoesNotReturnReverseRange_WhenMinimumFoundWithoutIteration()
		{
			throw new NotImplementedException();
		}

		[Test]
		public void FindRange_FindsRangeAtLeastOnceForAllCasesInSimpleParabolicFunction()
		{
			throw new InconclusiveException("Expansion method is not reliable, test fails for some cases.");
			var fullRange = new Range(-10, 10);
			for (var expansionParam = 1.1; expansionParam < 2.0; expansionParam += 0.01)
			{
				for (var startingPoint = -10.0; startingPoint <= 10; startingPoint += 0.1)
				{
					var rangeFinder = new RangeFinder(_parabolicFunction, expansionParam);
					var range = rangeFinder.Find(fullRange, startingPoint);
					Assert.That(range.ContainsInclusive(0),
						String.Format("Minimum not found in range {0} starting in {1} [actual range: {2}]", 
						fullRange, startingPoint, range));
				}
			}
		}
		#endregion

		#region Sophisticated function

		[TestCase(-10, 10)]
		[TestCase(4.8, 7.8)]
		public void FindRange_FindsRangeContainingProperValue_ForSophisticatedFunction(double rangeStart, double rangeEnd)
		{
			//Arrange
			var initialRange = new Range(rangeStart, rangeEnd);
			var rangeFinder = new RangeFinder(_sophisticatedFunction, 1.1);
			var startingPoint = SophisticatedFunctionProperValue;
			
			//Act
			var result = rangeFinder.Find(initialRange, SophisticatedFunctionProperValue);

			//Assert
			Assert.That(result.ContainsInclusive(SophisticatedFunctionProperValue), 
				String.Format("Minimum not found in range {0} starting in {1} [actual range: {2}]", initialRange, SophisticatedFunctionProperValue, result));
		}
		#endregion

		[Test]
		public void FindRange_ThrowsArgumentException_WhenStartingPointDoesNotBelongToRange()
		{
			//Arrange
			var range = GetRandomRange();
			var startingPoint = range.Start - 0.0001f;
			var rangeFinder = new RangeFinder(_parabolicFunction, 1.2);

			//Act
			Assert.Throws<ArgumentException>(() => rangeFinder.Find(range, startingPoint));
		}

		[Test]
		public void FindRange_ThrowsTooManyIterationsException_WhenMinimumNotFoundInConsiderableTime()
		{
			//Arrange
			var rangeFinder = new RangeFinder(_parabolicFunction, 1.0000000001);

			//Act
			Assert.Throws<IterationLimitExceededException>(() => rangeFinder.Find(new Range(-10000000, 1000000), -1000000));
		}

		#region Helpers

		private Range GetRandomRange()
		{
			return new Range(_random.NextDouble() * -100, _random.NextDouble() * 100);
		}

		private double GetRandomWithinRange(Range range)
		{
			return range.Start + (_random.NextDouble()*Math.Abs(range.End - range.Start));
		}

		#endregion

	}
}
