﻿using System;

namespace Optimizers.Models
{
	public class Point2D
	{
		public double X { get; set; }
		public double Y { get; set; }

		public Point2D(double x, double y)
		{
			X = x;
			Y = y;
		}

        public Point2D() { }

		public override string ToString()
		{
			return String.Format("({0}, {1})", X, Y);
		}

		public static Point2D operator *(int times, Point2D input)
		{
			return new Point2D { X = input.X * times, Y = input.Y * times };
		}

        public static Point2D operator *(double times, Point2D input)
        {
            return new Point2D { X = input.X * times, Y = input.Y * times };
        }

		public static Point2D operator -(Point2D input1, Point2D input2)
		{
			return new Point2D { X = input1.X - input2.X, Y = input1.Y - input2.Y };
		}

        public static Point2D operator +(Point2D input1, Point2D input2)
        {
            return new Point2D { X = input1.X + input2.X, Y = input1.Y + input2.Y };
        }

        public static Point2D operator -(Point2D input1)
        {
            return new Point2D { X = -input1.X, Y = -input1.Y };
        }
	}
}
