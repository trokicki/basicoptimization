using System;

namespace Optimizers.Models
{
	public class Point2DFunction : IPoint2DFunction
	{
        public Func<Point2D, double> Function { get; set; }
 
		public Point2DFunction(Func<Point2D, double> function )
		{
			Function = function;
		}
	}
}