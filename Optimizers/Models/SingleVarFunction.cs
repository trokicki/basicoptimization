﻿using System;

namespace Optimizers.Models
{
	public class SingleVarFunction : ISingleVarFunction
	{
		public Func<double, double> Function { get; set; }

		public SingleVarFunction(Func<double, double> function)
		{
			Function = function;
		}
	}
}