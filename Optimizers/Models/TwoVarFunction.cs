using System;

namespace Optimizers.Models
{
	public class TwoVarFunction : ITwoVarFunction
	{
        public Func<double, double, double> Function { get; set; }

		public TwoVarFunction(Func<double, double, double> function)
		{
			Function = function;
		}
	}
}