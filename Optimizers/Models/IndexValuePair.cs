namespace Optimizers.Models
{
	public class IndexValuePair<TIndex, TValue>
	{
		public TIndex Index { get; private set; }
		public TValue Value { get; private set; }

		public IndexValuePair(TIndex index, TValue value)
		{
			Index = index;
			Value = value;
		}
	}
}