﻿namespace Optimizers.Models
{
	public class OptimizationResult<T>
	{
        public int StepsCount { get; private set; }
        public T OptimizedValue { get; private set; }

		public OptimizationResult() { }

		public OptimizationResult(T currentValue, int performedSteps) : this()
		{
			OptimizedValue = currentValue;
			StepsCount = performedSteps;
		}
	}
}