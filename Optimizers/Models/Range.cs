using System.Runtime.InteropServices.WindowsRuntime;

namespace Optimizers.Models
{
	public class Range
	{
		public double Start { get; set; }
		public double End { get; set; }

		public double Length
		{
			get { return End - Start; }
		}

		public Range(double start, double end) : this()
		{
			Start = start;
			End = end;
		}

        public Range() {  }

		public Range(Range range)
		{
			Start = range.Start;
			End = range.End;
		}

		public override string ToString()
		{
			return string.Format("[{0}, {1}]", Start, End);
		}

		public bool ContainsInclusive(double value)
		{
			return (value >= Start && value <= End);
		}

		public static Range Normalized(Range range)
		{
			return range.End < range.Start ? new Range(range.End, range.Start) : range;
		}

		public static Range Normalized(double start, double end)
		{
			return end < start ? new Range(end, start) : new Range(start, end);
		}
	}

	public static class RangeExtensions
	{
		public static bool IsValid(this Range range)
		{
			return range.Start <= range.End;
		}
	}
}