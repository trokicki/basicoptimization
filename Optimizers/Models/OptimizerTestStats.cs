using System.Collections.Generic;

namespace Optimizers.Models
{
	public class OptimizerTestStats
	{
		public List<int> Steps { get; set; }
		public List<double> Errors { get; set; }

		public OptimizerTestStats()
		{
			Steps = new List<int>();
			Errors = new List<double>();
		}
	}
}