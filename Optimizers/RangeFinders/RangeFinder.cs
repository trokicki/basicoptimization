using System;
using System.Runtime.InteropServices;
using Optimizers.Exceptions;
using Optimizers.Models;

namespace Optimizers.RangeFinders
{
	public class RangeFinder : IRangeFinder
	{
		private readonly ISingleVarFunction _function;
		private readonly double _expansionParam;
		private const int MaxIterations = 1000;
		private const double Precision = 0.000001;

		public RangeFinder(ISingleVarFunction function, double expansionParam)
		{
			_function = function;
			_expansionParam = expansionParam;
		}

		public Range Find(Range initialRange, double startingPoint)
		{
			if (!initialRange.ContainsInclusive(startingPoint))
			{
				throw new ArgumentException(
					   String.Format("StartingPoint {0} does not belong to range {1}",
					   startingPoint, initialRange));
			}

			//TODO: test (does not work properly)
			var function = _function.Function;
			var steps = 0;

			var x0 = startingPoint;
			var x1 = startingPoint + 2; //Temporary

			if (Math.Abs(function(x1) - function(x0)) < Precision)
				return new Range(x0, x1);

			if (function(x1) > function(x0))
			{
				x1 -= 2 * (x1 - x0);
				if (function(x1) >= function(x0))
				{
					return Range.Normalized(x0, -x0);
				}
			}

			var dX = Math.Abs(x1*_expansionParam - x1);
			while (steps <= MaxIterations)
			{
				steps++;
				dX *= _expansionParam;
				var xNext = x1 + dX;
				if (function(x1) <= function(xNext))
				{
					return Range.Normalized(x0, xNext);
				}
				x0 = x1;
				x1 = xNext;
				if (x1 < x0) throw new Exception();
			}
			throw new IterationLimitExceededException();
		}

	}
}