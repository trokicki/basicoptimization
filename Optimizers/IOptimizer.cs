namespace Optimizers
{
	public interface IOptimizer
	{
		string Name { get; }
	}
}
