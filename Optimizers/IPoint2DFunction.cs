using System;
using Optimizers.Models;

namespace Optimizers
{
	public interface IPoint2DFunction
	{
		Func<Point2D, double> Function { get; } 
	}
}