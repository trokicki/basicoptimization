using Optimizers.Models;

namespace Optimizers
{
	public interface IBinarySearch
	{
		double SearchXFor(double functionValue, Range range);
	}
}