﻿using Optimizers.Exceptions;
using Optimizers.Helpers;
using Optimizers.Models;
using Optimizers.Optimizers.SingleVariable;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Optimizers.Optimizers.MultipleVariables
{
    public class NewtonOptimizer : ITwoVarsOptimizer
    {
        public string Name { get { return "NewtonOptimizer"; } }

        private double _epsilon { get; set; }
        private static readonly Random random = new Random();
        private readonly IPoint2DFunction _function;
        private readonly IPoint2DFunction[] _functionGradient;

        public NewtonOptimizer(IPoint2DFunction function, IPoint2DFunction[] functionGradient, double epsilon)
        {
            _epsilon = epsilon;
            _function = function;
            _functionGradient = functionGradient;
        }

        private static int GetRange(double range, int multiplayer)
        {
            return (int)(range * multiplayer);
        }

        public OptimizationResult<Point2D> Optimize(Range x1Range, Range x2Range)
        {
            int maxSteps = 1000, i = 1;
            double H = 1;
            var point = new Point2D
            {
                X = (double)random.Next(GetRange(x1Range.Start, 100), GetRange(x1Range.End, 100)) / 100,
                Y = (double)random.Next(GetRange(x2Range.Start, 100), GetRange(x2Range.End, 100)) / 100
            };
            var point2 = new Point2D();
            var pointD = new Point2D();


            while (i < maxSteps)
            {
                pointD = GetD(point);
				H = GetH(point, pointD, x1Range, x2Range);
				point2 = point + H * pointD;
                if ((GetLength(point - point2) < _epsilon))
                {
                    return new OptimizationResult<Point2D>(point, i);
                }
                point = point2;
                i++;
            }
			throw new IterationLimitExceededException();
        }
        
        private Point2D GetD(Point2D point)
        {
            Point2D result = new Point2D();

            //Hesian
            Func<Point2D, double>[,] Hesian = GetHesian();
            //Hesian^-1
            double[,] output = ReverseHesian(Hesian, point);

            var xx = _functionGradient[0].Function(point);
            var yy = _functionGradient[1].Function(point);

            if (xx == 0 && yy == 0)
            {
                throw new GradientEqalsZeroException();// a moze jednak bedzie lepiej po prostu zakonczyc petle ?
            }
            result.X = Hesian[0, 0](point) * xx + Hesian[0, 1](point) * yy;
            result.Y = Hesian[1, 0](point) * xx + Hesian[1, 1](point) * yy;

            return -result;
        }

        private double[,] ReverseHesian(Func<Point2D, double>[,] Hesian, Point2D p)
        {
            double[,] result = new double[2, 2];
            for (int i = 0; i < 2; i++)
            {
                for (int j = 0; j < 2; j++)
                {
                    result[i, j] = Hesian[i, j](p);
                }
            }
            double det = result[0, 0] * result[1, 1] - result[0, 1] * result[1, 0];
            if (det == 0)
            {
                throw new DetEqaulsZeroException(); // a moze jednak bedzie lepiej po prostu zakonczyc petle ?
            }

            //filled matrix
            double[,] resultF = new double[2, 2];

            resultF[0, 0] = result[1, 1];
            resultF[0, 1] = -1 * result[1, 0];
            resultF[1, 0] = -1 * result[0, 1];
            resultF[1, 1] = result[0, 0];

            for (int i = 0; i < 2; i++)
            {
                for (int j = 0; j < 2; j++)
                {
                    result[i, j] = resultF[i, j] * 1 / det;
                }
            }
            return result;
        }

        public Func<Point2D, double>[,] GetHesian()
        {
            Func<Point2D, double>[,] Hesian = { { p => 6 *p.X *Math.Pow(p.Y,2) + 4*p.X - 4.5*Math.Pow(p.Y,2)
                                                + 2* p.X * Math.Pow(p.Y,4) +  4*p.X*Math.Pow(p.Y,3)
                                                + 2* p.X * Math.Pow(p.Y,6) + 6,
                                                p => 12*p.X*p.Y + 4*p.X -9*p.Y + 8*p.X*Math.Pow(p.Y, 3)
                                                + 12*p.X*Math.Pow(p.Y,2) + 12*p.X*Math.Pow(p.Y,5)
                                                - 15.9*Math.Pow(p.Y,2) -3 },
                                                { p => 12*p.X*p.Y + 4*p.X -9*p.Y + 8*p.X*Math.Pow(p.Y, 3)
                                                + 12*p.X*Math.Pow(p.Y,2) + 12*p.X*Math.Pow(p.Y,5)
                                                - 15.9*Math.Pow(p.Y,2) -3,
                                                p => 6*Math.Pow(p.X,2) - 9*p.X + 12*Math.Pow(p.X,2)*Math.Pow(p.Y,2)
                                                + 12*Math.Pow(p.X,2)*p.Y + 30*Math.Pow(p.X,2)*Math.Pow(p.Y,4)
                                                -31.8*p.X*p.Y }
                                              };

            return Hesian;
        }

        private double GetLength(Point2D point)
        {
            return Math.Sqrt(Math.Pow(point.X, 2) + Math.Pow(point.Y, 2));
        }

        private double GetH(Point2D point, Point2D pointD, Range rangeX, Range rangeY)
        {
            //(point.X + alfa * pointD.X)
            //(point.Y + alfa * pointD.Y)
            //one big ass function...just to calculate alfa >.<

            var fun = new SingleVarFunction(alfa =>
                2.25 - 3 * (point.X + alfa * pointD.X) + Math.Pow((point.X + alfa * pointD.X), 2)
                - 3 * (point.X + alfa * pointD.X) * (point.Y + alfa * pointD.Y)
                + 2 * Math.Pow((point.X + alfa * pointD.X), 2) * (point.Y + alfa * pointD.Y)
                + Math.Pow((point.X + alfa * pointD.X), 2) * Math.Pow((point.Y + alfa * pointD.Y), 2)
                + 5.0625 - 4.5 * (point.X + alfa * pointD.X) + Math.Pow((point.X + alfa * pointD.X), 2)
                - 4.5 * (point.X + alfa * pointD.X) * Math.Pow((point.Y + alfa * pointD.Y), 2)
                + 2 * Math.Pow((point.X + alfa * pointD.X), 2) * Math.Pow((point.Y + alfa * pointD.Y), 2)
                + Math.Pow((point.X + alfa * pointD.X), 2) * Math.Pow((point.Y + alfa * pointD.Y), 4)
                + 7.0225 - 5.3 * (point.X + alfa * pointD.X) + Math.Pow((point.X + alfa * pointD.X), 2)
                - 5.3 * (point.X + alfa * pointD.X) * Math.Pow((point.Y + alfa * pointD.Y), 3)
                + Math.Pow((point.X + alfa * pointD.X), 2) * Math.Pow((point.Y + alfa * pointD.Y), 6)
            );

            //and the end of it...

            //new X and Y must have restrictions just like original ones
            Range range = new Range()
            {
                Start = ((rangeX.Start - point.X) / pointD.X) > ((rangeY.Start - point.Y) / pointD.Y) ?
                  ((rangeX.Start - point.X) / pointD.X) : ((rangeY.Start - point.Y) / pointD.Y),
                End = ((rangeX.End - point.X) / pointD.X) < ((rangeY.End - point.Y) / pointD.Y) ?
                  ((rangeX.End - point.X) / pointD.X) : ((rangeY.End - point.Y) / pointD.Y)
            };

            range = Range.Normalized(range);
            //we have SingleVarFunction and range... lets optimize !

            var optimizer = new GoldenDivisionSingleVarOptimizer(fun, epsilon: 0.01);
            var startingPoint = StartingPointOptimizer.NewPoint(range, random);
            var result = optimizer.Optimize(range.Start, range.End);

            return result.OptimizedValue;
        }
    }

}
