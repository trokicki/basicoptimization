﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Optimizers.Models;

namespace Optimizers.Optimizers.MultipleVariables
{
	public class NaiveTwoVarsOptimizer : ITwoVarsOptimizer
	{
		public string Name { get { return "Naive"; } }
		private IPoint2DFunction _function;

		public NaiveTwoVarsOptimizer(IPoint2DFunction function)
		{
			_function = function;
		}

		public OptimizationResult<Point2D> Optimize(Range x1Range, Range x2Range)
		{
			var step = 0.001;
			var bestPoint = new Point2D(x1Range.Start, x2Range.Start);
			var bestValue = Double.MaxValue;
			var function = _function.Function;
			for (var x = x1Range.Start; x < x1Range.End; x += step)
			{
				for (var y = x2Range.Start; y < x2Range.End; y += step)
				{
					var point = new Point2D(x, y);
					var value = function(point);
                    if (value < bestValue)
                    {
	                    bestPoint = point;
	                    bestValue = value;
                    }
				}
			}
			return new OptimizationResult<Point2D>(bestPoint, -1);
		}
	}
}
