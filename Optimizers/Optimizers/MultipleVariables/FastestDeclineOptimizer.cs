﻿using Optimizers.Exceptions;
using Optimizers.Helpers;
using Optimizers.Models;
using Optimizers.Optimizers.SingleVariable;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Optimizers.Optimizers.MultipleVariables
{
    public class FastestDeclineOptimizer : ITwoVarsOptimizer
    {
        public string Name { get { return "FastestDeclineOptimizer"; } }
        private readonly IPoint2DFunction _function;
        private readonly IPoint2DFunction[] _functionGradient;

        private readonly double _epsilon;
        private static readonly Random random = new Random();

        public double Function(Point2D point)
        {
            return _function.Function(point);
        }

        public FastestDeclineOptimizer(IPoint2DFunction function, IPoint2DFunction[] functionGradient, double epsilon)
        {
            _epsilon = epsilon;
            _function = function;
            _functionGradient = functionGradient;
        }

        private static int GetRange(double range, int multiplayer)
        {
            return (int)(range * multiplayer);
        }

        public OptimizationResult<Point2D> Optimize(Range x1Range, Range x2Range)
        {
            
            int maxSteps = 1000, i = 1;
            double H = 1;
            var point = new Point2D
            {
                X = (double)random.Next(GetRange(x1Range.Start, 100), GetRange(x1Range.End, 100)) / 100,
                Y = (double)random.Next(GetRange(x2Range.Start, 100), GetRange(x2Range.End, 100)) / 100
            };
            var point2 = new Point2D();
            var pointD = new Point2D();


            while (i < maxSteps)
            {
                pointD = GetD(point);
                H = GetH(point, pointD, x1Range, x2Range);
                point2 = point + H * pointD;
                if ((GetLength(point - point2) < _epsilon))
                {
                    return new OptimizationResult<Point2D>(point2, i);
                }
                point = point2;
                i++;
            }
			throw new IterationLimitExceededException();
        }

        private double GetH(Point2D point, Point2D pointD, Range rangeX, Range rangeY)
        {
            //(point.X + alfa * pointD.X)
            //(point.Y + alfa * pointD.Y)
            //one big ass function...just to calculate alfa >.<

            var fun = new SingleVarFunction(alfa =>
                2.25 - 3 * (point.X + alfa * pointD.X) + Math.Pow((point.X + alfa * pointD.X), 2)
                - 3 * (point.X + alfa * pointD.X) * (point.Y + alfa * pointD.Y)
                + 2 * Math.Pow((point.X + alfa * pointD.X), 2) * (point.Y + alfa * pointD.Y)
                + Math.Pow((point.X + alfa * pointD.X), 2) * Math.Pow((point.Y + alfa * pointD.Y), 2)
                + 5.0625 - 4.5 * (point.X + alfa * pointD.X) + Math.Pow((point.X + alfa * pointD.X), 2)
                - 4.5 * (point.X + alfa * pointD.X) * Math.Pow((point.Y + alfa * pointD.Y), 2)
                + 2 * Math.Pow((point.X + alfa * pointD.X), 2) * Math.Pow((point.Y + alfa * pointD.Y), 2)
                + Math.Pow((point.X + alfa * pointD.X), 2) * Math.Pow((point.Y + alfa * pointD.Y), 4)
                + 7.0225 - 5.3 * (point.X + alfa * pointD.X) + Math.Pow((point.X + alfa * pointD.X), 2)
                - 5.3 * (point.X + alfa * pointD.X) * Math.Pow((point.Y + alfa * pointD.Y), 3)
                + Math.Pow((point.X + alfa * pointD.X), 2) * Math.Pow((point.Y + alfa * pointD.Y), 6)
            );

            //and the end of it...

            //new X and Y must have restrictions just like original ones
            Range range = new Range()
            {
                Start = ((rangeX.Start - point.X) / pointD.X) > ((rangeY.Start - point.Y) / pointD.Y) ?
                  ((rangeX.Start - point.X) / pointD.X) : ((rangeY.Start - point.Y) / pointD.Y),
                End = ((rangeX.End - point.X) / pointD.X) < ((rangeY.End - point.Y) / pointD.Y) ?
                  ((rangeX.End - point.X) / pointD.X) : ((rangeY.End - point.Y) / pointD.Y)
            };

            range = Range.Normalized(range);
            //we have SingleVarFunction and range... lets optimize !

            var optimizer = new GoldenDivisionSingleVarOptimizer(fun, epsilon: 0.01);
            var startingPoint = StartingPointOptimizer.NewPoint(range, random);
            var result = optimizer.Optimize(range.Start, range.End);

            return result.OptimizedValue;
        }

        private Point2D GetD(Point2D point)
        {
            Point2D result = new Point2D();

            result.X = _functionGradient[0].Function(point);
            result.Y = _functionGradient[1].Function(point);

            return -result;
        }

        private double GetLength(Point2D point)
        {
            return Math.Sqrt(Math.Pow(point.X, 2) + Math.Pow(point.Y, 2));
        }
    }
}
