﻿using Optimizers.Exceptions;
using Optimizers.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Optimizers.Optimizers.MultipleVariables
{
	public enum aFlag
	{
		jeden,
		dwa,
		trzy
	}

	public class SinfulHookeJeevesOptimizer : ITwoVarsOptimizer
	{
		private const int Dimensions = 2;
		private readonly double _epsilon; //dokladnosc
		private readonly double _alpha; // o ile zmieniac step po nieudanym przejsciu petli
		private aFlag _aFlag;
		private bool _sinFlag;
		private double _step = 0.5;
		private readonly IPoint2DFunction _function;
		private const int MaxIterations = 1000000;
		private static readonly Random Random = new Random();

		private bool _isBeamTest;

		public SinfulHookeJeevesOptimizer(IPoint2DFunction function, double alpha, double epsilon)
		{
			_function = function;
			_alpha = alpha;
			_epsilon = epsilon;
		}

		public SinfulHookeJeevesOptimizer(IPoint2DFunction function, double alpha, double epsilon, bool isBeamTest)
			: this(function, alpha, epsilon)
		{
			_isBeamTest = isBeamTest;
		}

		public SinfulHookeJeevesOptimizer(IPoint2DFunction function, double alpha, double epsilon, bool sinFlag, aFlag aflag)
			: this(function, alpha, epsilon)
		{
			_aFlag = aflag;
			_sinFlag = sinFlag;
		}

		public string Name { get { return "Sin Hooke-Jeeves"; } }

		public double External(Point2D point)
		{
			double a = 1;
			switch (_aFlag)
			{
				case aFlag.jeden: a = 4; break;
				case aFlag.dwa: a = 4.4934; break;
				case aFlag.trzy: a = 5; break;
				default: break;
			}

			Func<double, double> del12 = x => { return -x + 1; };
			Func<Point2D, double, double> del3 = (p, aLocal) => { return Math.Sqrt(Math.Pow(p.X, 2) + Math.Pow(p.X, 2)) - aLocal; };
			double result = 0;
			if (del12(point.X) > 0) result += del12(point.X);
			if (del12(point.Y) > 0) result += del12(point.Y);
			if (del3(point, a) > 0) result += del3(point, a);

			return result;
		}

		public double Internal(Point2D point)
		{
			double a = 1;
			switch (_aFlag)
			{
				case aFlag.jeden: a = 4; break;
				case aFlag.dwa: a = 4.4934; break;
				case aFlag.trzy: a = 5; break;
				default: break;
			}
			Func<double, double> del12 = x => { return -x + 1; };
			Func<Point2D, double, double> del3 = (p, aLocal) => { return Math.Sqrt(Math.Pow(p.X, 2) + Math.Pow(p.X, 2)) - aLocal; };

			return 1 / del12(point.X) + 1 / del12(point.Y) + 1 / del3(point, a);
		}

		private double BeamSin(Point2D point)
		{
            const double YoungModule = 207000000;
			const double force = 1000;

            Func<double, double> g1 = d => -d + 0.01;
            Func<double, double> g2 = d => d - 0.05;
            Func<double, double> g3 = l => -l + 0.2;
            Func<double, double> g4 = l => l - 1;
            var naprezenieFunction = new TwoVarFunction((d, l) => (32 * force * l) / (Math.PI * Math.Pow(d, 3)) - 300000);
            var naprezenie = naprezenieFunction.Function(point.X, point.Y);
            var ugiecieFunction = new TwoVarFunction((d, l) => 64 * force * Math.Pow(l, 3) / (3 * YoungModule * Math.PI * Math.Pow(d, 4)) - 0.005);
            var ugiecie = ugiecieFunction.Function(point.X, point.Y);
            //double kara = -(1 / g1(point.X) + 1 / g2(point.X) + 1 / g3(point.Y) + 1 / g4(point.Y) + 1 / naprezenie + 1 / ugiecie);
            double kara = 0;
            if (g1(point.X) > 0) kara += g1(point.X);
            if (g2(point.X) > 0) kara += g2(point.X);
            if (g3(point.Y) > 0) kara += g3(point.Y);
            if (g4(point.Y) > 0) kara += g4(point.Y);
            if (naprezenie > 0) kara += naprezenie;
            if (ugiecie > 0) kara += ugiecie;
            return kara;
		}

		public double functionWithSin(Point2D point, int steps)
		{
			if (_isBeamTest)
			{
				return _function.Function(point) +  Math.Pow(0.5, steps)*BeamSin(point);
			}

			double sin = _sinFlag ? (Math.Pow(0.5, steps) * Internal(point)) : (Math.Pow(2, steps) * External(point));
			return _function.Function(point) + sin;
		}

		public OptimizationResult<Point2D> Optimize(Range x1Range, Range x2Range)
		{
			if (!(x1Range.IsValid() && x2Range.IsValid()))
				throw new InvalidRangeException();

			_step = 0.05;
			var steps = 1;
			var startingPoint = new Point2D
			{
				X = (double)Random.Next(GetRange(x1Range.Start, 100), GetRange(x1Range.End, 100)) / 100,
				Y = (double)Random.Next(GetRange(x2Range.Start, 100), GetRange(x2Range.End, 100)) / 100
			};

			var basePoint = startingPoint;

			while (_step > _epsilon)
			{
				basePoint = startingPoint;
				startingPoint = TestSample(basePoint, _step, steps);

				if (_function.Function(startingPoint) < _function.Function(basePoint))
				{
					while (_function.Function(startingPoint) < _function.Function(basePoint))
					{
						var tmpBase = basePoint;
						basePoint = startingPoint;
						startingPoint = 2 * basePoint - tmpBase; // working sample
						startingPoint = TestSample(startingPoint, _step, steps);
					}
					startingPoint = basePoint;
				}
				else
				{
					_step = _alpha * _step;
				}
				steps++;
				if (steps > MaxIterations)
					throw new IterationLimitExceededException();
			}

			return new OptimizationResult<Point2D>(startingPoint, steps);
		}

		private Point2D TestSample(Point2D point, double step, int steps)
		{
            double newX = point.X, newY = point.Y;
			for (var i = 1; i <= Dimensions; i++)
			{
				if (i == 1)
				{
                    if (functionWithSin(new Point2D() { X = newX + step/10, Y = newY }, steps) < functionWithSin(point, steps))
					{
                        newX = newX + step/10;
					}
                    else if (functionWithSin(new Point2D() { X = newX - step/10, Y = newY }, steps) < functionWithSin(point, steps))
					{
                        newX = newX - step/10;
					}
				}
				else if (i == 2)
				{
					if (functionWithSin(new Point2D() { X = newX, Y = newY + step }, steps) < functionWithSin(point, steps))
					{
						newY = newY + step;
					}
					else if (functionWithSin(new Point2D() { X = newX, Y = newY - step }, steps) < functionWithSin(point, steps))
					{
                        newY = newY - step;
					}
				}
			}
            return new Point2D(newX, newY);
		}

		private static int GetRange(double range, int multiplayer)
		{
			return (int)(range * multiplayer);
		}
	}
}
