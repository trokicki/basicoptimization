﻿using System;
using Optimizers.Models;

namespace Optimizers.Optimizers.MultipleVariables
{
	public class RosenbroockOptimizer : ITwoVarsOptimizer
	{
		private readonly IPoint2DFunction _function;
		private readonly int _iterationsAmount;
		private readonly double _alpha;
		private readonly double _beta;
		public const int Dimensions = 2;
		private static readonly Random Random = new Random();

		public double Function(Point2D point)
		{
			return _function.Function(point);
		}

		public RosenbroockOptimizer(IPoint2DFunction function, int iterationsAmount, double alpha, double beta)
		{
			_function = function;
			_iterationsAmount = iterationsAmount;
			_alpha = alpha;
			_beta = beta;
		}

		public int GetRange(double range, int multiplayer)
		{
			return (int)(range * multiplayer);
		}

		public double[] NewDirections(double[] Q1, double[] d)
		{
			double tmp = Q1[0] * d[0] + Q1[1] * d[1];
			for (int i = 0; i < Dimensions; i++)
			{
				d[i] *= tmp;
				Q1[i] -= d[i];
			}
			return Q1;
		}

		public OptimizationResult<Point2D> Optimize(Range x1Range, Range x2Range)
		{
			var step = new[] { 0.5, 0.5 };
			var failures = new[] { 0.0, 0.0 };
			var goneSoFar = new[] { 0.0, 0.0 };
			var directions = new[] { 1.0, 1.0 };

			var basePoint = new Point2D
			{
				X = (double)Random.Next(GetRange(x1Range.Start, 100), GetRange(x1Range.End, 100)) / 100,
				Y = (double)Random.Next(GetRange(x2Range.Start, 100), GetRange(x2Range.End, 100)) / 100
			};

			var steps = 0;
			while (steps < _iterationsAmount)
			{
				for (int i = 0; i < Dimensions; i++)
				{
					if (i == 0)
					{
						if (Function(new Point2D() { X = basePoint.X + step[i] * directions[i], Y = basePoint.Y }) < Function(basePoint))
						{
							basePoint = new Point2D() { X = basePoint.X + step[i] * directions[i], Y = basePoint.Y };
							goneSoFar[i] += step[i];
							step[i] = _alpha * step[i];
						}
						else
						{
							step[i] = (-1) * _beta * step[i];
							failures[i]++;
						}
					}
					else if (i == 1)
					{
						if (Function(new Point2D() { X = basePoint.X, Y = basePoint.Y + step[i] * directions[i] }) < Function(basePoint))
						{
							basePoint = new Point2D() { X = basePoint.X, Y = basePoint.Y + step[i] * directions[i] };
							goneSoFar[i] += step[i];
							step[i] = _alpha * step[i];
						}
						else
						{
							step[i] = (-1) * _beta * step[i];
							failures[i]++;
						}
					}
				}

				steps++;
				if (goneSoFar[0] != 0 && goneSoFar[1] != 0 && failures[0] != 0 && failures[1] != 0)
				{
					directions = NewDirections(goneSoFar, directions);
					for (int i = 0; i < Dimensions; i++)
					{
						goneSoFar[i] = 0;
						failures[i] = 0;
						step[i] = 0.5;
					}
				}
			}
			return new OptimizationResult<Point2D>(basePoint, steps);
		}

		public string Name { get { return "Rosenbrock"; } }
	}
}
