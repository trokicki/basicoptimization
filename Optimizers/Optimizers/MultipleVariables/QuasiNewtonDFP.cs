﻿using Optimizers.Helpers;
using Optimizers.Models;
using Optimizers.Optimizers.SingleVariable;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Optimizers.Exceptions;

namespace Optimizers.Optimizers.MultipleVariables
{
    public class QuasiNewtonDFP : ITwoVarsOptimizer
    {
        public string Name { get { return "QuasiNewtonDFP"; } }

        private double _epsilon { get; set; }
        private static readonly Random random = new Random();
        private readonly IPoint2DFunction _function;
        private readonly IPoint2DFunction[] _functionGradient;
        private double[,] prevV { get; set; }

        public QuasiNewtonDFP(IPoint2DFunction function, IPoint2DFunction[] functionGradient, double epsilon)
        {
            _epsilon = epsilon;
            _function = function;
            _functionGradient = functionGradient;
        }

        private static int GetRange(double range, int multiplayer)
        {
            return (int)(range * multiplayer);
        }

        public OptimizationResult<Point2D> Optimize(Range x1Range, Range x2Range)
        {
            int maxSteps = 1000, i = 1;
            double H = 1;
            var newPoint = new Point2D
            {
                X = (double)random.Next(GetRange(x1Range.Start, 100), GetRange(x1Range.End, 100)) / 100,
                Y = (double)random.Next(GetRange(x2Range.Start, 100), GetRange(x2Range.End, 100)) / 100
            };
            var oldPoint = new Point2D
            {
                X = (double)random.Next(GetRange(x1Range.Start, 100), GetRange(x1Range.End, 100)) / 100,
                Y = (double)random.Next(GetRange(x2Range.Start, 100), GetRange(x2Range.End, 100)) / 100
            };
            var point2 = new Point2D();
            var pointD = new Point2D();

            prevV = new double[2, 2] { { 1, 0 }, { 0, 1 } };

            while (i < maxSteps)
            {
                pointD = GetD(newPoint, oldPoint);
                H = GetH(newPoint,pointD,x1Range,x2Range);
                point2 = newPoint + H * pointD;
				if (GetLength(newPoint - point2) < _epsilon)
				{
					return new OptimizationResult<Point2D>(newPoint, i);
				}
				oldPoint = newPoint;
                newPoint = point2;
				i++;
            }
			throw new IterationLimitExceededException();
		}

        private Point2D GetD(Point2D point, Point2D point2)
        {
            Point2D result = new Point2D();
            double[,] newV = new double[2, 2];

            Point2D a = new Point2D() { X = point.X - point2.X, Y = point.Y - point2.Y };
            Point2D s = new Point2D()
            {
                X = _functionGradient[0].Function(point) - _functionGradient[0].Function(point2),
                Y = _functionGradient[1].Function(point) - _functionGradient[1].Function(point2)
            };
            //calculate A
            double tmp = a.X * s.X + a.Y + s.Y;
            double[,] A = new double[2, 2];
            A[0, 0] = a.X * a.X / tmp;
            A[0, 1] = a.X * a.Y / tmp;
            A[1, 0] = a.Y * a.X / tmp;
            A[1, 1] = a.Y * a.Y / tmp;

            //calculate B
            tmp = (s.X * prevV[0, 0] + s.Y * prevV[1, 0]) * s.X + (s.X * prevV[0, 1] + s.Y * prevV[1, 1]) * s.Y;
            double[] VS = new double[2];
            VS[0] = s.X * prevV[0, 0] + s.Y * prevV[0, 1];
            VS[1] = s.X * prevV[1, 0] + s.Y * prevV[1, 1];
            double[] SV = new double[2];
            SV[0] = s.X * prevV[0, 0] + s.Y * prevV[1, 0];
            SV[1] = s.X * prevV[0, 1] + s.Y * prevV[1, 1];
            double[,] B = new double[2, 2];
            B[0, 0] = -VS[0] * SV[0] / tmp;
            B[1, 0] = -VS[1] * SV[0] / tmp;
            B[0, 1] = -VS[0] * SV[1] / tmp;
            B[1, 1] = -VS[1] * SV[1] / tmp;

            //calculate V
            for (int i = 0; i < 2; i++)
            {
                for (int j = 0; j < 2; j++)
                {
                    newV[i, j] = prevV[i, j] + A[i, j] + B[i, j];
                }
            }

            prevV = newV;

            var xx = _functionGradient[0].Function(point);
            var yy = _functionGradient[1].Function(point);
            result.X = newV[0, 0] * xx + newV[0, 1] * yy;
            result.Y = newV[1, 0] * xx + newV[1, 1] * yy;

            return -result;
        }

        private double GetLength(Point2D point)
        {
            return Math.Sqrt(Math.Pow(point.X, 2) + Math.Pow(point.Y, 2));
        }

        private double GetH(Point2D point, Point2D pointD, Range rangeX, Range rangeY)
        {
            //(point.X + alfa * pointD.X)
            //(point.Y + alfa * pointD.Y)
            //one big ass function...just to calculate alfa >.<

            var fun = new SingleVarFunction(alfa =>
                2.25 - 3 * (point.X + alfa * pointD.X) + Math.Pow((point.X + alfa * pointD.X), 2)
                - 3 * (point.X + alfa * pointD.X) * (point.Y + alfa * pointD.Y)
                + 2 * Math.Pow((point.X + alfa * pointD.X), 2) * (point.Y + alfa * pointD.Y)
                + Math.Pow((point.X + alfa * pointD.X), 2) * Math.Pow((point.Y + alfa * pointD.Y), 2)
                + 5.0625 - 4.5 * (point.X + alfa * pointD.X) + Math.Pow((point.X + alfa * pointD.X), 2)
                - 4.5 * (point.X + alfa * pointD.X) * Math.Pow((point.Y + alfa * pointD.Y), 2)
                + 2 * Math.Pow((point.X + alfa * pointD.X), 2) * Math.Pow((point.Y + alfa * pointD.Y), 2)
                + Math.Pow((point.X + alfa * pointD.X), 2) * Math.Pow((point.Y + alfa * pointD.Y), 4)
                + 7.0225 - 5.3 * (point.X + alfa * pointD.X) + Math.Pow((point.X + alfa * pointD.X), 2)
                - 5.3 * (point.X + alfa * pointD.X) * Math.Pow((point.Y + alfa * pointD.Y), 3)
                + Math.Pow((point.X + alfa * pointD.X), 2) * Math.Pow((point.Y + alfa * pointD.Y), 6)
            );

            //and the end of it...

            //new X and Y must have restrictions just like original ones
            Range range = new Range()
            {
                Start = ((rangeX.Start - point.X) / pointD.X) > ((rangeY.Start - point.Y) / pointD.Y) ?
                  ((rangeX.Start - point.X) / pointD.X) : ((rangeY.Start - point.Y) / pointD.Y),
                End = ((rangeX.End - point.X) / pointD.X) < ((rangeY.End - point.Y) / pointD.Y) ?
                  ((rangeX.End - point.X) / pointD.X) : ((rangeY.End - point.Y) / pointD.Y)
            };

            range = Range.Normalized(range);
            //we have SingleVarFunction and range... lets optimize !

            var optimizer = new GoldenDivisionSingleVarOptimizer(fun, epsilon: 0.01);
            var startingPoint = StartingPointOptimizer.NewPoint(range, random);
            var result = optimizer.Optimize(range.Start, range.End);

            return result.OptimizedValue;
        }
    }
}
