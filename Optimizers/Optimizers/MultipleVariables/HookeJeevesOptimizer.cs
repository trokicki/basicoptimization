﻿using System;
using Optimizers.Exceptions;
using Optimizers.Models;

namespace Optimizers.Optimizers.MultipleVariables
{
    public class HookeJeevesOptimizer : ITwoVarsOptimizer
    {
	    private const int Dimensions = 2;
	    private readonly double _epsilon; //dokladnosc
	    private readonly double _alpha; // o ile zmieniac step po nieudanym przejsciu petli
        private double _step = 0.5;
	    private readonly IPoint2DFunction _function;
		private const int MaxIterations = 1000000;
	    private static readonly Random Random = new Random();

	    public HookeJeevesOptimizer(IPoint2DFunction function, double alpha, double epsilon)
	    {
		    _function = function;
		    _alpha = alpha;
		    _epsilon = epsilon;
	    }
		
	    public string Name { get { return "Hooke-Jeeves"; } }

	    public OptimizationResult<Point2D> Optimize(Range x1Range, Range x2Range)
	    {
			if(!(x1Range.IsValid() && x2Range.IsValid()))
				throw new InvalidRangeException();

		    _step = 0.5;
		    var steps = 0;
		    var function = _function.Function;
		    var startingPoint = new Point2D()
		    {
			    X = (double) Random.Next(GetRange(x1Range.Start, 100), GetRange(x1Range.End, 100))/100,
			    Y = (double) Random.Next(GetRange(x2Range.Start, 100), GetRange(x2Range.End, 100))/100
		    };

		    var basePoint = startingPoint;

			while (_step > _epsilon)
			{
				basePoint = startingPoint;
				startingPoint = TestSample(basePoint, _step);

				if (function(startingPoint) < function(basePoint))
				{
					while (function(startingPoint) < function(basePoint))
					{
						var tmpBase = basePoint;
						basePoint = startingPoint;
						startingPoint = 2 * basePoint - tmpBase; // working sample
						startingPoint = TestSample(startingPoint, _step);
					}
					startingPoint = basePoint;
				}
				else
				{
					_step = _alpha * _step;
				}
				steps++;
				if(steps > MaxIterations)
					throw new IterationLimitExceededException();
			}
			
		    return new OptimizationResult<Point2D>(basePoint, steps);
		}

		private Point2D TestSample(Point2D point, double step)
		{
			var result = point;
			var function = _function.Function;

			for (var i = 1; i <= Dimensions; i++)
			{
				if (i == 1)
				{
					if (function(new Point2D() { X = point.X + step, Y = point.Y }) < function(point))
					{
						result = new Point2D() { X = point.X + step, Y = point.Y };
					}
					else if (function(new Point2D() { X = point.X - step, Y = point.Y }) < function(point))
					{
						result = new Point2D() { X = point.X - step, Y = point.Y };
					}
				}
				else if (i == 2)
				{
					if (function(new Point2D() { X = point.X, Y = point.Y + step }) < function(point))
					{
						result = new Point2D() { X = point.X, Y = point.Y + step };
					}
					else if (function(new Point2D() { X = point.X, Y = point.Y - step }) < function(point))
					{
						result = new Point2D() { X = point.X, Y = point.Y - step };
					}
				}
			}

			return result;
		}

		private static int GetRange(double range, int multiplayer)
		{
			return (int)(range * multiplayer);
		}
	}
}
