using System;
using Optimizers.Exceptions;
using Optimizers.Models;

namespace Optimizers.Optimizers.SingleVariable
{
	public class LagrangeInterpolationSingleVarOptimizer : ISingleVarOptimizer
	{
		private readonly ISingleVarFunction _function;
		private readonly double _epsilon;
		private readonly double _gamma;
		private const int MaxSteps = 1000;

		public string Name { get { return "Lagrange interpolation"; } }

		public LagrangeInterpolationSingleVarOptimizer(ISingleVarFunction function, double epsilon, double gamma)
		{
			_function = function;
			_epsilon = epsilon;
			_gamma = gamma;
		}

		public OptimizationResult<double> Optimize(double rangeStart, double rangeEnd)
		{
			if (rangeStart > rangeEnd)
				throw new InvalidRangeException();

			var a = rangeStart;
			var b = rangeEnd;
			var c = CalculateCPoint(a, b);
			var d = 0.0;
			double oldD;

			var f = _function.Function;
			var steps = 0;
			do
			{
				oldD = d;
				d = 0.5 * 
					((f(a) * (Math.Pow(c, 2) - Math.Pow(b, 2)) 
					+ f(c) * (Math.Pow(b, 2) - Math.Pow(a, 2)) 
					+ f(b) * (Math.Pow(a, 2) - Math.Pow(c, 2))))
					/
					(f(a) * (c - b) + f(c) * (b - a) + f(b) * (a - c));

				if (a < d && d < c)
				{
					if (f(d) < f(c))
					{
						c = d;
						b = c;
					}
					else
					{
						a = d;
					}
				}
				else
				{
					if (c < d && d < b)
					{
						if (f(d) < f(c))
						{
							a = c;
							c = d;
						}
						else
						{
							b = d;
						}
					}
					else
					{
						throw new InconclusiveOptimizationException("Will not work for this function in given range.");
					}
				}

				steps++;
				if (steps > MaxSteps)
					throw new IterationLimitExceededException();
			} while (b - a >= _epsilon && Math.Abs(d - oldD) > _gamma);

			return new OptimizationResult<double>(d, steps);
		}

		private double CalculateCPoint(double rangeStart, double rangeEnd)
		{
			return (rangeEnd + rangeStart) / 2;
		}
	}
}
