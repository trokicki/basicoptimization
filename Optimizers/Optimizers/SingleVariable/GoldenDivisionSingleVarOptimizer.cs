using System;
using Optimizers.Exceptions;
using Optimizers.Models;

namespace Optimizers.Optimizers.SingleVariable
{
	public class GoldenDivisionSingleVarOptimizer : ISingleVarOptimizer
	{
		public string Name { get { return "GoldenDivision"; } }
		private readonly double _kParam = (Math.Sqrt(5) - 1) / 2;
		private readonly double _epsilon;
		private readonly ISingleVarFunction _function;

		private const int MaxIterations = 1000;

		public GoldenDivisionSingleVarOptimizer(ISingleVarFunction function, double epsilon)
		{
			_function = function;
			_epsilon = epsilon;
		}

		public OptimizationResult<double> Optimize(double rangeStart, double rangeEnd)
		{
			var steps = 0;
			if (rangeStart > rangeEnd)
				throw new InvalidRangeException();

			var function = _function.Function;

			var xL = rangeEnd - _kParam * (rangeEnd - rangeStart);
			var xR = rangeStart + _kParam * (rangeEnd - rangeStart);

			while ((rangeEnd - rangeStart) > _epsilon)
			{
				if (function(xL) < function(xR))
				{
					rangeEnd = xR;
					xR = xL;
					xL = rangeEnd - _kParam * (rangeEnd - rangeStart);
				}
				else
				{
					rangeStart = xL;
					xL = xR;
					xR = rangeStart + _kParam * (rangeEnd - rangeStart);
				}
				steps++;
				if (steps > MaxIterations)
					throw new IterationLimitExceededException();
			}
			return new OptimizationResult<double>((rangeStart + rangeEnd) / 2, steps);
		}
	}
}