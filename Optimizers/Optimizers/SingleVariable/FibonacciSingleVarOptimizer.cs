using System.Linq;
using Optimizers.Exceptions;
using Optimizers.Helpers;
using Optimizers.Models;

namespace Optimizers.Optimizers.SingleVariable
{
	public class FibonacciSingleVarOptimizer : ISingleVarOptimizer
	{
		public string Name { get { return "Fibonacci"; } }
		private ISingleVarFunction _function;
		private readonly double _epsilon;
		private readonly FibonacciNumbersProvider _fibonacciNumbersProvider;

		public FibonacciSingleVarOptimizer(ISingleVarFunction function, double epsilon, FibonacciNumbersProvider fibonacciNumbersProvider)
		{
			_function = function;
			_epsilon = epsilon;
			_fibonacciNumbersProvider = fibonacciNumbersProvider;
		}

		public OptimizationResult<double> Optimize(double rangeStart, double rangeEnd)
		{
			if (rangeStart > rangeEnd)
				throw new InvalidRangeException();
			var steps = 1;

			var kParam = GetParamForFirstFibonacciItemGreaterThan((rangeEnd - rangeStart) / _epsilon);
            var cPoint = rangeEnd - (GetFibonacciNumber(kParam - 1) / GetFibonacciNumber(kParam)) * (rangeEnd - rangeStart);
			var dPoint = rangeStart + rangeEnd - cPoint;
			
			for (var i = 0; i < kParam - 4; i++)
			{
				if (_function.Function(cPoint) < _function.Function(dPoint))
				{
					rangeEnd = dPoint;
				}
				else
				{
					rangeStart = cPoint;
				}
				cPoint = rangeEnd - (GetFibonacciNumber(kParam - i - 2)/GetFibonacciNumber(kParam - i - 1))*(rangeEnd - rangeStart);
				dPoint = rangeStart + rangeEnd - cPoint;
				steps++;
			}
			return new OptimizationResult<double>(cPoint, steps);
		}

		private int GetFibonacciNumber(int index)
		{
			return _fibonacciNumbersProvider.GetNumber(index);
		}

		private int GetParamForFirstFibonacciItemGreaterThan(double val)
		{
			return val >= 1.0 ? _fibonacciNumbersProvider.GetWhile((index, item) => item <= val).Last().Index : 1;
		}
	}
}