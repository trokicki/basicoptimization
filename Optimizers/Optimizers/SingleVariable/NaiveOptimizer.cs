using System;
using Optimizers.Models;

namespace Optimizers.Optimizers.SingleVariable
{
	public class NaiveSingleVarOptimizer : ISingleVarOptimizer
	{
	    public string Name { get { return "Naive optimizer";  } }

		private Func<double, double> Function { get; set; }
		private readonly double _resolution;

		public NaiveSingleVarOptimizer(ISingleVarFunction function, double resolution)
		{
			Function = function.Function;
			_resolution = resolution;
		}

		public OptimizationResult<double> Optimize(double rangeStart, double rangeEnd)
		{
		    var minimumX = rangeStart;
		    var minimumValue = Function(rangeStart);
			for (var x = rangeStart + _resolution; x < rangeEnd; x += _resolution)
			{
				var value = Function(x);
			    if (value >= minimumValue) 
                    continue;

			    minimumValue = value;
			    minimumX = x;
			}
			
			return new OptimizationResult<double>(minimumX, (int)((rangeEnd - rangeStart) / _resolution));
		}
	}
}