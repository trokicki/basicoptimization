using System;

namespace Optimizers
{
	public interface ITwoVarFunction
	{
		Func<double, double, double> Function { get; } 
	}
}