using Optimizers.Models;

namespace Optimizers
{
	public interface ITwoVarsOptimizer : IOptimizer
	{
		OptimizationResult<Point2D> Optimize(Range x1Range, Range x2Range);
	}
}