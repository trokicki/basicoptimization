using Optimizers.Models;

namespace Optimizers
{
	public interface ISingleVarOptimizer : IOptimizer
	{
		OptimizationResult<double> Optimize(double rangeStart, double rangeEnd);
	}
}