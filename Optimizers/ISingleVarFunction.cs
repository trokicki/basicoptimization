using System;

namespace Optimizers
{
	public interface ISingleVarFunction
	{
		Func<double, double> Function { get; } 
	}
}