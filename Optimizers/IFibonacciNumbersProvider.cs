using System;
using System.Collections.Generic;
using Optimizers.Models;

namespace Optimizers
{
	public interface IFibonacciNumbersProvider
	{
		int GetNumber(int index);
		IEnumerable<IndexValuePair<int, int>> GetWhile(Func<int, int, bool> func);
	}
}