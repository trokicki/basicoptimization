﻿using System;
using System.Net.Http.Headers;
using Optimizers.Exceptions;
using Optimizers.Models;

namespace Optimizers.Helpers
{
	public class BinarySearch : IBinarySearch
	{
		private readonly ISingleVarFunction _function;
		private readonly double _epsilon;
		private const int MaxIterations = 1000;

		public BinarySearch(ISingleVarFunction function, double epsilon = 0.001)
		{
			_function = function;
			_epsilon = epsilon;
		}

		public double SearchXFor(double functionValue, Range range)
		{
			var steps = 0;
			var arises = IsFunctionArising(range);
			while (true)
			{
				var middle = (range.End + range.Start) / 2;
				var middleValue = _function.Function(middle);
				if (Math.Abs(middleValue - functionValue) < _epsilon)
					return middle;

				if (middleValue > functionValue && arises || middleValue < functionValue && !arises)
					range.End = middle;
				else
					range.Start = middle;
				steps++;
				if (steps == MaxIterations)
					throw new IterationLimitExceededException();
			}
		}

		private bool IsFunctionArising(Range range)
		{
			return _function.Function(range.End) > _function.Function(range.Start);
		}
	}
}
