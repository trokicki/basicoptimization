﻿using Optimizers.Models;
using System;

namespace Optimizers.Helpers
{
    public class StartingPointOptimizer
    {
        public static double NewPoint(Range initialRange, Random random)
        {
            return initialRange.Start + (random.NextDouble() * initialRange.End - initialRange.Start);
        }

        public static Point2D NewPoint(Range initialRangeX1, Range initialRangeX2, Random random)
        {
            var x1 = NewPoint(initialRangeX1, random);
            var x2 = NewPoint(initialRangeX2, random);
            return new Point2D(x1, x2);
        }
    }
}
