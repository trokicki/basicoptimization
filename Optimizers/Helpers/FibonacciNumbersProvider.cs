﻿using System;
using System.Collections.Generic;
using System.Linq;
using Optimizers.Models;

namespace Optimizers.Helpers
{
	public class FibonacciNumbersProvider : IFibonacciNumbersProvider
	{
		public int GetNumber(int index)
		{
			return GetWhile((ind, item) => ind <= index).Last().Value;
		}
		
		public IEnumerable<IndexValuePair<int, int>> GetWhile(Func<int, int, bool> func)
		{
			var items = new[] {1, 1};
			var index = 0;
			while (func(index, items[1]))
			{
				if (index <= 1)
					yield return new IndexValuePair<int, int>(index, items[index]);
				else
				{
					var newItem = items[0] + items[1];
					items[0] = items[1];
					items[1] = newItem;
					yield return new IndexValuePair<int, int>(index, items[1]);
				}
				index++;
			}
		} 
	}
}
