﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Optimizers.Exceptions
{
    public class GradientEqalsZeroException : Exception
    {
        public GradientEqalsZeroException() : base() { }
        public GradientEqalsZeroException(string message) : base(message) { }
    }
}
