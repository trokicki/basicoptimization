using System;

namespace Optimizers.Exceptions
{
	public class InconclusiveOptimizationException : Exception
	{
		public InconclusiveOptimizationException() : base() { }
		public InconclusiveOptimizationException(string message) : base(message) { }
	}
}