﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Optimizers.Exceptions
{
    public class DetEqaulsZeroException : Exception
    {
        public DetEqaulsZeroException() : base() { }
        public DetEqaulsZeroException(string message) : base(message) { }
    }
}
