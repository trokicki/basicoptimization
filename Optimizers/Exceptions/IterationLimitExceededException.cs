﻿using System;

namespace Optimizers.Exceptions
{
	public class IterationLimitExceededException : Exception { }
}