using Optimizers.Models;

namespace Optimizers
{
	interface IRangeFinder
	{
		Range Find(Range initialRange, double startingPoint);
	}
}