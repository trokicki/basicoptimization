﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using NLog;
using Optimizers;
using Optimizers.Exceptions;
using Optimizers.Models;
using Optimizers.RangeFinders;
using Optimizers.Helpers;
using Optimizers.Optimizers.MultipleVariables;
using Optimizers.Optimizers.SingleVariable;

namespace BasicOptimization
{
	public class Program
	{
		private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
		private static readonly Random Random = new Random();

		public static void Main(string[] args)
		{
			//CheckAndPresentTwoVarsGradientFunctionOptimizers(args);
			CheckAndPresentRealWorldExample(args);
		}

		#region helpers

		private static void LogOptimizersStats(Dictionary<IOptimizer, OptimizerTestStats> optimizersStats)
		{
			foreach (var keyValuePair in optimizersStats)
			{
				Logger.Info("Collected stats of {0}", keyValuePair.Key.Name);
				if (keyValuePair.Value.Steps.Any())
				{
					Logger.Info("- Steps (fastest | average | slowest): {0} | {1} | {2}",
						keyValuePair.Value.Steps.Min(),
						keyValuePair.Value.Steps.Average(),
						keyValuePair.Value.Steps.Max()
						);
				}

				if (keyValuePair.Value.Errors.Any())
				{
					Logger.Info("- Deviation (smallest | average | biggest): {0} | {1} | {2}",
						keyValuePair.Value.Errors.Min(),
						keyValuePair.Value.Errors.Average(),
						keyValuePair.Value.Errors.Max());
				}
			}
		}

		private static void LogFinishedOptimization<T>(OptimizationResult<T> result, string optimizerName, double deviation)
		{
			Logger.Debug("- Optimizer: {0}. Minimum {1} found in {2} steps. Deviation: {3}",
				optimizerName,
				result.OptimizedValue,
				result.StepsCount,
				deviation);
		}

		private static double Calculate2DDeviation(Point2D pointA, Point2D pointB)
		{
			return Math.Sqrt(Math.Pow(Math.Abs(pointB.X - pointA.X), 2) + Math.Pow(Math.Abs(pointB.Y - pointA.Y), 2));
		}

		#endregion

		#region first classes 

		private static void CheckAndPresentSingleVarFunctionOptimizers(string[] args)
		{
			const double properValue = 6.27481807546061; //found naively with precision 10e-8
			var testsAmount = 100;

			if (args.Any())
				Int32.TryParse(args[0], out testsAmount);

			var fullRange = new Range(-10f, 10f);
			var random = new Random();
			var function = new SingleVarFunction(x => -Math.Cos(x) * Math.Exp(-Math.Pow(x - 2 * Math.PI, 2)) + 0.002 * Math.Pow(x, 2));
			var rangeFinder = new RangeFinder(function, expansionParam: 1.1);
			var fibonacciNumbersProvider = new FibonacciNumbersProvider();

			var optimizers = new ISingleVarOptimizer[]
			{
				new GoldenDivisionSingleVarOptimizer(function, epsilon: 0.01),
				new FibonacciSingleVarOptimizer(function, epsilon: 2, fibonacciNumbersProvider: fibonacciNumbersProvider),
				new LagrangeInterpolationSingleVarOptimizer(function, epsilon: 0.001, gamma: 0.001)
			};

			var optimizersStats = optimizers.ToDictionary(optimizer => optimizer as IOptimizer, optimizer => new OptimizerTestStats());

			for (var i = 0; i < testsAmount; i++)
			{
				var startingPoint = StartingPointOptimizer.NewPoint(fullRange, random);
				var range = rangeFinder.Find(fullRange, startingPoint);
				Logger.Trace("Test {0} of {1}. Starting x = {2}. Finding in range {3}", i + 1, testsAmount, startingPoint, range);
				foreach (var optimizer in optimizers)
				{
					try
					{
						var result = optimizer.Optimize(range.Start, range.End);
						var deviation = Math.Abs(result.OptimizedValue - properValue);
						optimizersStats[optimizer].Steps.Add(result.StepsCount);
						optimizersStats[optimizer].Errors.Add(deviation);
						LogFinishedOptimization(result, optimizer.Name, deviation);
					}
					catch (IterationLimitExceededException e)
					{
						Logger.Error("- Optimizer: {0}. Optimization failed due to iteration limit overflow", optimizer.Name);
					}
					catch (InconclusiveOptimizationException e)
					{
						Logger.Error("- Optimizer: {0}. Optimization failed due to optimization error.", optimizer.Name);
					}
				}

			}
			Logger.Info("Done.");
			LogOptimizersStats(optimizersStats);
		}
		#endregion

		#region second classes
		private static void CheckAndPresentTwoVarsFunctionOptimizers(string[] args)
		{
			var random = new Random();
			var rangeX1 = new Range(-1, 1);
			var rangeX2 = new Range(-1, 1);
			var function = new Point2DFunction(point => Math.Pow(point.X, 2) + Math.Pow(point.Y, 2) - Math.Cos(2.5 * Math.PI * point.X) - Math.Cos(2.5 * Math.PI * point.Y) + 2);
			var testsCount = 100;

			var optimizers = new ITwoVarsOptimizer[]
			{
				new HookeJeevesOptimizer(function, alpha: 0.5, epsilon: 0.001),
				new RosenbroockOptimizer(function, iterationsAmount: 100, alpha: 2, beta: 0.5)
			};
			var optimizersStats = optimizers.ToDictionary(optimizer => optimizer as IOptimizer, optimizer => new OptimizerTestStats());

			for (var i = 0; i < testsCount; i++)
			{
				var startingPoint = StartingPointOptimizer.NewPoint(new Range(-1.0, 1.0), new Range(-1.0, 1.0), random);
				var bestValuePoint = new Point2D(0, 0);
				Logger.Trace("Test {0} of {1}. Starting point = {2}.", i + 1, testsCount, startingPoint);
				foreach (var optimizer in optimizers)
				{
					try
					{
						var result = optimizer.Optimize(rangeX1, rangeX2);
						var deviation = Calculate2DDeviation(result.OptimizedValue, bestValuePoint);
						optimizersStats[optimizer].Steps.Add(result.StepsCount);
						optimizersStats[optimizer].Errors.Add(deviation);
						LogFinishedOptimization(result, optimizer.Name, deviation: deviation);
					}
					catch (IterationLimitExceededException e)
					{
						Logger.Error("- Optimizer: {0}. Optimization failed due to iteration limit overflow", optimizer.Name);
					}
				}
			}
			Logger.Info("Done.");
			LogOptimizersStats(optimizersStats);
		}

		#endregion

		#region third classes

		private static void CheckAndPresentTwoVarsSinFunctionOptimizers(string[] args)
		{
			var random = new Random();
			var testsCount = 100;
			var rangeX1 = new Range(-4, 4);
			var rangeX2 = new Range(-4, 4);
			var function = new Point2DFunction(point => Math.Sin(Math.PI * Math.Sqrt(Math.Pow(point.X / Math.PI, 2) + Math.Pow(point.Y / Math.PI, 2))) /
														(Math.PI * Math.Sqrt(Math.Pow(point.X / Math.PI, 2) + Math.Pow(point.Y / Math.PI, 2))));



			bool sinFlag = false;
			for (int k = 0; k < 2; k++)//sinFlag = true / false
			{
				foreach (var item in Enum.GetValues(typeof(aFlag)))
				{
					var optimizer = new SinfulHookeJeevesOptimizer(function, alpha: 0.5, epsilon: 0.001, sinFlag: k > 0, aflag: (aFlag)item);
					var optimizersStats = new Dictionary<IOptimizer, OptimizerTestStats>();
					optimizersStats.Add(optimizer as IOptimizer, new OptimizerTestStats());
					for (var i = 0; i < testsCount; i++)//iterations
					{
						var startingPoint = StartingPointOptimizer.NewPoint(new Range(-4.0, 4.0), new Range(-4.0, 4.0), random);
						var bestValuePoint = new Point2D(0, 0);
						Logger.Trace("Test {0} of {1}. Starting point = {2}.", i + 1, testsCount, startingPoint);

						try
						{
							var result = optimizer.Optimize(rangeX1, rangeX2);
							var deviation = Calculate2DDeviation(result.OptimizedValue, bestValuePoint);
							optimizersStats[optimizer].Steps.Add(result.StepsCount);
							optimizersStats[optimizer].Errors.Add(deviation);
							LogFinishedOptimization(result, optimizer.Name, deviation: deviation);
						}
						catch (IterationLimitExceededException e)
						{
							Logger.Error("- Optimizer: {0}. Optimization failed due to iteration limit overflow", optimizer.Name);
						}
					}
				}

			}
			Logger.Info("Done.");
			//LogOptimizersStats(optimizersStats);
		}

		#endregion

		#region fourth classes
		private static void CheckAndPresentTwoVarsGradientFunctionOptimizers(string[] args)
		{
			var testsCount = 100;
			var rangeX1 = new Range(-4, 4);
			var rangeX2 = new Range(-4, 4);
			var function = new Point2DFunction(point => Math.Pow((1.5 - point.X + point.X * point.Y), 2) +
														  Math.Pow((2.25 - point.X + point.X * point.Y * point.Y), 2) +
														  Math.Pow((2.65 - point.X + point.X * point.Y * point.Y * point.Y), 2));
			var functionGradient = new Point2DFunction[2];
			functionGradient[0] = new Point2DFunction(point => 6 * point.X * Math.Pow(point.Y, 2) + 4 * point.X * point.Y - 4.5 * Math.Pow(point.Y, 2) +
																2 * point.X * Math.Pow(point.Y, 4) + 4 * point.X * Math.Pow(point.Y, 3) +
																2 * point.X * Math.Pow(point.Y, 6) - 5.3 * Math.Pow(point.Y, 3) - 3 * point.Y +
																6 * point.X - 12.8);
			functionGradient[1] = new Point2DFunction(point => 6 * point.Y * Math.Pow(point.X, 2) + 2 * Math.Pow(point.X, 2) - 9 * point.Y * point.X +
																4 * Math.Pow(point.X, 2) * Math.Pow(point.Y, 3) + 6 * Math.Pow(point.X, 2) * Math.Pow(point.Y, 2) +
																6 * Math.Pow(point.X, 2) * Math.Pow(point.Y, 5) - 15.9 * point.X * Math.Pow(point.Y, 2) - 3 * point.X);

			var optimizers = new ITwoVarsOptimizer[]
			{
				new FastestDeclineOptimizer(function, functionGradient, epsilon: 0.001),
				new NewtonOptimizer(function, functionGradient, epsilon: 0.001),
				new QuasiNewtonDFP(function, functionGradient, epsilon: 0.001)
			};

			var optimizersStats = optimizers.ToDictionary(optimizer => optimizer as IOptimizer, optimizer => new OptimizerTestStats());
			var bestValuePoint = new Point2D(3, 0.5);

			var filePath = "table.txt";
			for (var i = 0; i < testsCount; i++)
			{
				var startingPoint = StartingPointOptimizer.NewPoint(new Range(-4.0, 4.0), new Range(-4.0, 4.0), Random);
				Logger.Trace("Test {0} of {1}. Starting point = {2}.", i + 1, testsCount, startingPoint);
				foreach (var optimizer in optimizers)
				{
					try
					{
						var result = optimizer.Optimize(rangeX1, rangeX2);
						var deviation = Calculate2DDeviation(result.OptimizedValue, bestValuePoint);
						optimizersStats[optimizer].Steps.Add(result.StepsCount);
						optimizersStats[optimizer].Errors.Add(deviation);
						LogFinishedOptimization(result, optimizer.Name, deviation: deviation);
						File.AppendAllText(filePath, String.Format("{0}\t", result.OptimizedValue));
					}
					catch (IterationLimitExceededException e)
					{
						Logger.Error("- Optimizer: {0}. Optimization failed due to iteration limit overflow", optimizer.Name);
					}
					catch (Exception e)
					{
						Logger.Error("Cannot handle: {0}", e.Message);
					}
				}
				File.AppendAllText(filePath, "\n");
			}
			Logger.Info("Done.");
			LogOptimizersStats(optimizersStats);
		}
		#endregion

		#region fifth classes

		private const double YoungModule = 207e9;
		private const double Density = 7800; //kg/m3

		private static void CheckAndPresentRealWorldExample(string[] args)
		{
			//Point2D is treated as pair of diameter and length.
			// point.X - diameter
			// point.Y - length
			const int force = 1000; //N
			
			//Dodatkowe ograniczenia wewnątrz optymalizatora:
			// - maksymalne ugięcie belki = 5mm
			// - maksymalne naprezenie = 300MPa
			
			var diameterRange = new Range(0.01, 0.05);
			var lengthRange = new Range(0.2, 1);
			var outputFileBuilder = new StringBuilder();
			for (var weight = 0.01; weight <= 1.01; weight += 0.01)
			{
				var toBeOptimizedFunction = new Point2DFunction(point => weight * UgiecieFunction(point, force) + (1 - weight) * MassFunction(point));
				var optimizer = new SinfulHookeJeevesOptimizer(toBeOptimizedFunction, 0.5, 0.001, isBeamTest: true);
				var result = optimizer.Optimize(diameterRange, lengthRange);
				var diameter = result.OptimizedValue.X;
				var length = result.OptimizedValue.Y;
				Console.WriteLine("Weight: {0:N2} found value: {{ srednica : {1} ; dlugosc: {2} }}", weight, diameter, length);
				outputFileBuilder.AppendLine(weight + "\t" + diameter + "\t" + length);
			}
			File.WriteAllText("output.txt", outputFileBuilder.ToString());
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="point">X - diameter; Y - length</param>
		/// <param name="force"></param>
		/// <returns></returns>
		private static double UgiecieFunction(Point2D point, double force)
		{
			//point.X - średnica
			//point.Y - długość
			return 64 * force * Math.Pow(point.Y, 3) / (3 * YoungModule * Math.PI * Math.Pow(point.X, 4));
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="point">X - diameter; Y - length</param>
		/// <returns></returns>
		private static double MassFunction(Point2D point)
		{
			var volume = point.Y * Math.PI * Math.Pow(point.X, 2) / 4;
			var mass = volume * Density;
			return mass;
		}

		#endregion

		#region Runtime tests

		private static void RangeFinderTest()
		{
			var hits = 0;
			var tests = 0;
			var function = new SingleVarFunction(x => -Math.Cos(x) * Math.Exp(-Math.Pow(x - 2 * Math.PI, 2)) + 0.002 * Math.Pow(x, 2));
			var fullRange = new Range(-10, 10);
			const double properValue = 6.27481807546061;

			var bestExpansionParams = new List<double>();
			var hitsForBestExpansionParam = 0;

			for (var expansionParam = 1.01; expansionParam < 8.0; expansionParam += 0.01)
			{
				var rangeFinder = new RangeFinder(function, expansionParam);
				var hitsPerExpansionParam = 0;
				for (var startingPoint = -10.0; startingPoint <= 10.0; startingPoint += 0.1)
				{
					var range = rangeFinder.Find(fullRange, startingPoint);
					if (range.ContainsInclusive(properValue))
					{
						hitsPerExpansionParam++;
					}
					tests++;
				}
				if (hitsPerExpansionParam >= hitsForBestExpansionParam)
				{
					if (hitsPerExpansionParam > hitsForBestExpansionParam)
						bestExpansionParams.Clear();
					bestExpansionParams.Add(expansionParam);
					hitsForBestExpansionParam = hitsPerExpansionParam;
				}
				Console.WriteLine("Hits for expansionParam= {0} : {1}", expansionParam, hitsPerExpansionParam);
				hits += hitsPerExpansionParam;
			}

			Logger.Info("Hits: {0} of {1} [{2:p}]. Best result for expansionParam={3} {4}",
				hits, tests, (double)hits / tests, bestExpansionParams.First(), bestExpansionParams.Count > 1 ? "[and multiple others]" : "");
		}

		private static void FindMinimumNaively(ISingleVarFunction function, Range fullRange, double properValue)
		{
			var stopwatch = Stopwatch.StartNew();
			var naiveOptimizer = new NaiveSingleVarOptimizer(function, 0.00000001);
			var naiveResults = naiveOptimizer.Optimize(fullRange.Start, fullRange.End);
			var elapsed = stopwatch.Elapsed;
			var deviation = Math.Abs(naiveResults.OptimizedValue - properValue);
			LogFinishedOptimization(naiveResults, "Naive", deviation);
			Console.WriteLine("Calculation time: {0}", elapsed);
		}
		#endregion
	}
}
